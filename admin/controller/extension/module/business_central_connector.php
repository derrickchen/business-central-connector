<?php
class ControllerExtensionModuleBusinessCentralConnector extends Controller {
    private $error = array();

    private $endpoint = 'https://api.businesscentral.dynamics.com/v1.0/api/v1.0/companies';

    public function index() {
        $this->load->language('extension/module/business_central_connector');

        $this->load->model('setting/setting');

        $this->document->setTitle($this->language->get('heading_title'));

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('module_business_central_connector', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true),
            'separator' => false
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_extension'),
            'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true),
            'separator' => ' :: '
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/business_central_connector', 'user_token=' . $this->session->data['user_token'], true),
            'separator' => ' :: '
        );

        if (isset($this->request->post['module_business_central_connector_status'])) {
			$data['module_business_central_connector_status'] = $this->request->post['module_business_central_connector_status'];
		} elseif ($this->config->get('module_business_central_connector_status')) {
            $data['module_business_central_connector_status'] = $this->config->get('module_business_central_connector_status');
        }  else {
			$data['module_business_central_connector_status'] = $this->config->get('module_business_central_connector_status');
		}

        if (isset($this->request->post['module_business_central_connector_client_id'])) {
            $data['module_business_central_connector_client_id'] = $this->request->post['module_business_central_connector_client_id'];
        } elseif ($this->config->get('module_business_central_connector_client_id')) {
            $data['module_business_central_connector_client_id'] = $this->config->get('module_business_central_connector_client_id');
        }  else {
            $data['module_business_central_connector_client_id'] = $this->config->get('module_business_central_connector_client_id');
        }

        if (isset($this->request->post['module_business_central_connector_client_secret'])) {
            $data['module_business_central_connector_client_secret'] = $this->request->post['module_business_central_connector_client_secret'];
        } elseif ($this->config->get('module_business_central_connector_client_secret')) {
            $data['module_business_central_connector_client_secret'] = $this->config->get('module_business_central_connector_client_secret');
        }  else {
            $data['module_business_central_connector_client_secret'] = $this->config->get('module_business_central_connector_client_secret');
        }

        if (isset($this->request->post['module_business_central_connector_redirect_url'])) {
            $data['module_business_central_connector_redirect_url'] = $this->request->post['module_business_central_connector_redirect_url'];
        } elseif ($this->config->get('module_business_central_connector_redirect_url')) {
            $data['module_business_central_connector_redirect_url'] = $this->config->get('module_business_central_connector_redirect_url');
        } else {
            $data['module_business_central_connector_redirect_url'] = $this->config->get('module_business_central_connector_redirect_url');
        }

        if ($this->config->get('module_business_central_connector_company')) {
            $this->load->model('extension/module/business_central_connector');
            $company = $this->model_extension_module_business_central_connector->CallApi($this->endpoint . '(' . $this->config->get('module_business_central_connector_company') . ')');

            $data['module_business_central_connector_company'] = $company;
        } else {
            $data['module_business_central_connector_company'] = '';
        }

        $data['action'] = $this->url->link('extension/module/business_central_connector', 'user_token=' . $this->session->data['user_token'], true);
        $data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);
        $data['token'] = $this->url->link('extension/module/business_central_connector/getToken', 'user_token=' . $this->session->data['user_token'], true);

        $data['user_token'] = $this->session->data['user_token'];

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('extension/module/business_central_connector', $data));
    }

    public function getToken() {
        // Initialize the OAuth client
        $provider = new \League\OAuth2\Client\Provider\GenericProvider([
            'clientId'                => $this->config->get('module_business_central_connector_client_id'),            // The client ID assigned to you by the provider
            'clientSecret'            => $this->config->get('module_business_central_connector_client_secret'),        // The client password assigned to you by the provider
            'redirectUri'             => $this->config->get('module_business_central_connector_redirect_url'),
            'urlAuthorize'            => $this->config->get('module_business_central_connector_url_authorize'),
            'urlAccessToken'          => $this->config->get('module_business_central_connector_url_accesstoken'),
            'urlResourceOwnerDetails' => '',
        ]);

        $authUrl = $provider->getAuthorizationUrl();
        $this->session->data['oauthState'] = $provider->getState(); // Save client state so we can validate in callback

        // Redirect to AAD signin page
        header('Location: ' . $authUrl);
        exit;
    }

    public function getCompanies() {
        $this->load->model('extension/module/business_central_connector');

        $companies = $this->model_extension_module_business_central_connector->CallApi($this->endpoint);

        if ($companies['value']) {
            $json = $companies['value'];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function syncAll() {
        $this->load->model('extension/module/business_central_connector');

        $items = $this->model_extension_module_business_central_connector->CallApi($this->endpoint . '(' . $this->config->get('module_business_central_connector_company') . ')/items');
        echo "<pre>"; print_r($items); die("**");
    }

    public function sync() {
        $this->load->language('extension/module/business_central_connector');

        $json = array();

        if ($this->request->get['type'] == 'syncAll') {
            $this->load->model('extension/module/business_central_connector');

            $items = $this->model_extension_module_business_central_connector->CallApi($this->endpoint . '(' . $this->config->get('module_business_central_connector_company') . ')/items');

            foreach ($items['value'] as $item) {
                // Set product default value
                $product['model']		= $item['id'];
                $product['sku'] 		= '';
                $product['upc'] 		= $item['number'];
                $product['ean'] 		= '';
                $product['jan'] 		= '';
                $product['isbn'] 		= '';
                $product['mpn'] 		= '';
                $product['location'] 	= '';
                $product['price']		= $item['unitPrice'];
                $product['tax_class_id'] 	= 9;
                $product['quantity'] 		= $item['inventory'];
                $product['minimum'] 		= 0;
                $product['subtract'] 		= 1;
                $product['stock_status_id'] = 5;
                $product['shipping']		= 1;
                $product['date_available'] 	= date('Y-m-d');
                $product['length'] 			= '';
                $product['width'] 			= '';
                $product['height'] 			= '';
                $product['length_class_id'] = 1;
                $product['weight'] 			= '';
                $product['weight_class_id'] = '';
                $product['status'] 			= 1;
                $product['sort_order']		= '';
                $product['points']			= '';

                $product['product_store']		= array('0' => 0);
                $product['product_description'] = array(
                    '1'	=> array(
                        'name'			=> $item['displayName'],
                        'description'	=> '',
                        'meta_title'	=> $item['displayName'],
                        'meta_description'	=> '',
                        'meta_keyword'		=> '',
                        'tag'				=> ''
                    )
                );

                $this->model_catalog_product->addProduct($product);

                $json['success'] = $this->language->get('text_success');
            }

        } else if ($this->request->get['type'] == 'syncOne') {
            if (isset($this->request->post['item_id'])) {
                $item_id = $this->request->post['item_id'];
            } else {
                $item_id = '';
            }

            if (isset($this->request->post['item_number'])) {
                $item_number = $this->request->post['item_number'];
            } else {
                $item_number = '';
            }

            if (isset($this->request->post['item_name'])) {
                $item_name = $this->request->post['item_name'];
            } else {
                $item_name = '';
            }

            if (isset($this->request->post['unit_price'])) {
                $unit_price = $this->request->post['unit_price'];
            } else {
                $unit_price = '';
            }

            if (isset($this->request->post['quantity'])) {
                $quantity = $this->request->post['quantity'];
            } else {
                $quantity = '';
            }

            if ($item_id && $item_number && $item_name && $unit_price) {
                // Set product default value
                $product['model']		= $item_id;
                $product['sku'] 		= '';
                $product['upc'] 		= $item_number;
                $product['ean'] 		= '';
                $product['jan'] 		= '';
                $product['isbn'] 		= '';
                $product['mpn'] 		= '';
                $product['location'] 	= '';
                $product['price']		= $unit_price;
                $product['tax_class_id'] 	= 9;
                $product['quantity'] 		= $quantity;
                $product['minimum'] 		= 0;
                $product['subtract'] 		= 1;
                $product['stock_status_id'] = 5;
                $product['shipping']		= 1;
                $product['date_available'] 	= date('Y-m-d');
                $product['length'] 			= '';
                $product['width'] 			= '';
                $product['height'] 			= '';
                $product['length_class_id'] = 1;
                $product['weight'] 			= '';
                $product['weight_class_id'] = '';
                $product['status'] 			= 1;
                $product['sort_order']		= '';
                $product['points']			= '';
                $product['product_store']		= array('0' => 0);
                $product['product_description'] = array(
                    '1'	=> array(
                        'name'			=> $item_name,
                        'description'	=> '',
                        'meta_title'	=> $item_name,
                        'meta_description'	=> '',
                        'meta_keyword'		=> '',
                        'tag'				=> ''
                    )
                );

                $this->load->model('catalog/product');
                $this->model_catalog_product->addProduct($product);

                $json['success'] = $this->language->get('text_success');
            } else {
                $json['error'] = $this->language->get('text_error');
            }
        } else {
            $json['error'] = $this->language->get('text_error');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function autoComplete() {
        $json = array();

        if (isset($this->request->get['filter_name']) || isset($this->request->get['filter_number'])) {
            if (isset($this->request->get['filter_name'])) {
                $filter_name = $this->request->get['filter_name'];
            } else {
                $filter_name = '';
            }

            if (isset($this->request->get['filter_number'])) {
                $filter_number = $this->request->get['filter_number'];
            } else {
                $filter_number = '';
            }

            $this->load->model('extension/module/business_central_connector');

            if ($filter_name) {
                $item = $this->model_extension_module_business_central_connector->CallApi($this->endpoint . "(" . $this->config->get('module_business_central_connector_company') . ")/items?\$filter=displayName eq '" . $filter_name . "'");
            } else if ($filter_number) {
                $item = $this->model_extension_module_business_central_connector->CallApi($this->endpoint . "(" . $this->config->get('module_business_central_connector_company') . ")/items?\$filter=number eq '" . $filter_number . "'");
            }

            foreach ($item['value'] as $item) {
                $json[] = array(
                    'id'            => $item['id'],
                    'number'        => $item['number'],
                    'displayName'   => $item['displayName'],
                    'display'       => $item['number'] . '   ' . $item['displayName'],
                    'inventory'     => $item['inventory'],
                    'unitPrice'     => $item['unitPrice']
                );
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function install() {
        $this->load->model('setting/setting');
        $this->model_setting_setting->editSetting('module_business_central_connector',
            [
                'module_business_central_connector_status'          => 0,
                'module_business_central_connector_url_authorize'   => 'https://login.windows.net/common/oauth2/authorize?resource=https://api.businesscentral.dynamics.com',
                'module_business_central_connector_url_accesstoken' => 'https://login.windows.net/common/oauth2/token?resource=https://api.businesscentral.dynamics.com'
            ]);
    }

    public function uninstall() {
        $this->load->model('setting/setting');
        $this->model_setting_setting->deleteSetting('module_business_central_connector');
    }

    protected function validate() {
        if (!$this->user->hasPermission('modify', 'extension/module/business_central_connector')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
    }
}