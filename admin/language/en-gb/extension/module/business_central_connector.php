<?php

// Heading
$_['heading_title']     = 'Business Central Connector';

// Text
$_['text_extension']    = 'Extension';
$_['text_enable']       = 'Enable';
$_['text_disable']      = 'Disable';
$_['text_edit']         = 'Configuration Settings';
$_['text_info']         = 'Microsoft Business Central Connector Guide';
$_['text_info_li1']     = 'Authentication and authorization basics for Microsoft Graph - Refer to <a href="https://docs.microsoft.com/en-us/graph/auth/auth-concepts" target="_blank">https://docs.microsoft.com/en-us/graph/auth/auth-concepts</a>';
$_['text_info_li2']     = 'Before your app can get a token from the Microsoft identity platform, it must be registered in the <a href="https://portal.azure.com/" target="_blank">Azure portal</a>';
$_['text_info_li3']     = 'Register your app with the Microsoft identity platform and establishes the information includeing <b>Application (Client) ID</b>, <b>Redirect URI/URL(Must be localhost or https://)</b>, <b>Application (Client) Secret</b>';
$_['text_info_li4']     = 'Please set your redirect url as "$_["your_domain"]/index.php?route=business_central_connector/sync/callback"';
$_['text_info_li5']     = 'Click <b>get token</b> button to get access token after filling in all the parameters that required';
$_['text_info_li6']     = 'Click <b>Get Companies</b> button to get your business central companies after click the <b>get token</b> button';
$_['text_select_company']   = 'Please select company';
$_['text_sync_info']        = 'Synchronization Guide';
$_['text_info_li7']         = 'Item number and name must be fully typed in order to find the item from Business Central';
$_['text_info_li8']         = '<b>Sync item</b> button is to sync the searched item to Opencart';
$_['text_info_li9']         = '<b>Sync all item</b> button is to sync all items to Opencart';
$_['text_success']          = 'Sync item successful';
$_['text_error']            = 'Sync failed, please check your data and try again';

// Entry
$_['entry_status']          = 'Status';
$_['entry_client_id']       = 'Client ID';
$_['entry_client_secret']   = 'Client Secret';
$_['entry_redirect_url']    = 'Redirect URL';
$_['entry_company']         = 'Company List';
$_['entry_item_number']     = 'Search Item Number';
$_['entry_item_name']       = 'Search Item Name';
$_['entry_unit_price']      = 'Unit Price';
$_['entry_quantity']        = 'Quantity';

// Button
$_['button_token']      = 'Get Token';
$_['button_save']       = 'Save';
$_['button_cancel']     = 'Cancel';
$_['button_companies']  = 'Get Companies';
$_['button_sync_one']   = 'Sync item';
$_['button_sync_all']   = 'Sync all item';

// Error
$_['error_permission']  = 'You do not have permission!';

// Tab
$_['tab_general']       = 'General';
$_['tab_sync']          = 'Synchronization';

// Help
$_['help_client_id']        = 'A unique identifier assigned by the Microsoft identity platform';
$_['help_client_secret']    = 'A password or a public/private key pair that your app uses to authenticate with the Microsoft identity platform';
$_['help_redirect_url']     = 'One or more endpoints at which your app will receive responses from the Microsoft identity platform';
$_['help_company']          = 'Select your Business Central Company once you click <b>Get Companies</b> button';