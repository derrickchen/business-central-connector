<?php

class ModelExtensionModuleBusinessCentralConnector extends Model {
    private function getToken() {
        // Get Existing Access Token
        $token_query = $this->db->query("SELECT * 
                                           FROM " . DB_PREFIX . "bc_token 
                                       ORDER BY token_id DESC");

        $accessToken = array(
            'access_token'      => $token_query->row['access_token'],
            'refresh_token'     => $token_query->row['refresh_token'],
            'expires'           => $token_query->row['expires']
        );

        // Check if access token has expired
        if ($accessToken['expires'] < time()) {
            $provider = new \League\OAuth2\Client\Provider\GenericProvider([
                'clientId'                => $this->config->get('module_business_central_connector_client_id'),            // The client ID assigned to you by the provider
                'clientSecret'            => $this->config->get('module_business_central_connector_client_secret'),        // The client password assigned to you by the provider
                'redirectUri'             => $this->config->get('module_business_central_connector_redirect_url'),
                'urlAuthorize'            => $this->config->get('module_business_central_connector_url_authorize'),
                'urlAccessToken'          => $this->config->get('module_business_central_connector_url_accesstoken'),
                'urlResourceOwnerDetails' => '',
            ]);

            // Generate new access token by using refresh token
            $newAccessToken = $provider->getAccessToken('refresh_token', [
                'refresh_token' => $accessToken['refresh_token']
            ]);

            // Store New Access Token to DB
            $accessToken = array(
                'accessToken'	=> $newAccessToken->getToken(),
                'refreshToken'	=> $newAccessToken->getRefreshToken(),
                'expires'		=> $newAccessToken->getExpires()
            );

            $this->db->query("INSERT INTO " . DB_PREFIX . "bc_token 
                                  SET access_token = '" . $this->db->escape($accessToken['accessToken']) . "', 
                                      refresh_token = '" . $this->db->escape($accessToken['refreshToken']) . "', 
                                      expires = " . (int) $accessToken['expires']);
        }

        return $accessToken;
    }

    public function CallApi($url = false, $method = false, $data = false) {
        $accessToken = $this->getToken();

        if (!$method) {
            $method = 'GET';
        }

        try {
            // Create a Graph client
            $graph = new Microsoft\Graph\Graph();
            $graph->setAccessToken($accessToken['access_token']);

            try {
                if ($method == 'GET') {
                    // Create Get Request
                    $result = $graph->createRequest('GET', $url)->execute();
                } else {
                    // Create POST Request
                    $result = $graph->createRequest($method, $url)
                        ->attachBody($data)
                        ->execute();
                }
                return $result->getBody();

            } catch (\Microsoft\Graph\Exception\GraphException $e) {
                // Failed to call api
                die($e->getMessage());
            }
        } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
            // Failed to get the access token
            die($e->getMessage());

        }
    }
}