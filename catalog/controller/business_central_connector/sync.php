<?php

class ControllerBusinessCentralConnectorSync extends Controller {
    public function index() {

    }
    public function callback() {
        // Authorization code should be in the "code" query param
        $authCode = $this->request->get['code'];

        if (isset($authCode)) {
            // Initialize the OAuth client
            $oauthClient = new \League\OAuth2\Client\Provider\GenericProvider([
                'clientId'                => $this->config->get('module_business_central_connector_client_id'),            // The client ID assigned to you by the provider
                'clientSecret'            => $this->config->get('module_business_central_connector_client_secret'),        // The client password assigned to you by the provider
                'redirectUri'             => $this->config->get('module_business_central_connector_redirect_url'),
                'urlAuthorize'            => $this->config->get('module_business_central_connector_url_authorize'),
                'urlAccessToken'          => $this->config->get('module_business_central_connector_url_accesstoken'),
                'urlResourceOwnerDetails' => '',
            ]);

            try {
                // Make the token request
                $accessToken = $oauthClient->getAccessToken('authorization_code', [
                    'code' => $authCode
                ]);

                // Store access Token to Database
                $token = array(
                    'accessToken'	=> $accessToken->getToken(),
                    'refreshToken'	=> $accessToken->getRefreshToken(),
                    'expires'		=> $accessToken->getExpires()
                );

                $this->load->model('business_central_connector/sync');
                $this->model_business_central_connector_sync->addToken($token);

                // Store Accesstoken to session
                $this->session->data['accessToken'] = $accessToken;
                echo "<pre>"; print_r($accessToken); die("=====");

            } catch (League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
                die($e->getMessage());
            }
        }
    }
}