<?php

class ModelBusinessCentralConnectorSync extends Model {

    // Todo this function is to store business central oauth token into database
    // Parameters: $token, array
    public function addToken($token) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "bc_token 
                                  SET access_token = '" . $this->db->escape($token['accessToken']) . "', 
                                      refresh_token = '" . $this->db->escape($token['refreshToken']) . "', 
                                      expires = " . (int) $token['expires']);

    }

    // Todo this function is to get Token from database, generate new access token by using refresh token if access token has expired
    public function getToken() {
        $provider = new \League\OAuth2\Client\Provider\GenericProvider([
            'clientId'                => $this->config->get('module_business_central_connector_client_id'),            // The client ID assigned to you by the provider
            'clientSecret'            => $this->config->get('module_business_central_connector_client_secret'),        // The client password assigned to you by the provider
            'redirectUri'             => $this->config->get('module_business_central_connector_redirect_url'),
            'urlAuthorize'            => $this->config->get('module_business_central_connector_url_authorize'),
            'urlAccessToken'          => $this->config->get('module_business_central_connector_url_accesstoken'),
            'urlResourceOwnerDetails' => '',
        ]);

        $token_query = $this->db->query("SELECT * 
                                           FROM " . DB_PREFIX . "bc_token
                                       ORDER BY token_id DESC");

        $existingToken = array(
            'access_token'      => $token_query->row['access_token'],
            'refresh_token'     => $token_query->row['refresh_token'],
            'expires'           => $token_query->row['expires']
        );

        // Check if token has expired
        if ($existingToken['expires'] < time()) {
            $newAccessToken = $provider->getAccessToken('refresh_token', [
                'refresh_token' => $existingToken['refresh_token']
            ]);

            // Store New Access Token to DB
            $token = array(
                'accessToken'	=> $newAccessToken->getToken(),
                'refreshToken'	=> $newAccessToken->getRefreshToken(),
                'expires'		=> $newAccessToken->getExpires()
            );

            $this->addToken($token);
            $this->session->data['accessToken'] = $token;
        } else {
            $this->session->data['accessToken'] = $existingToken;
        }
    }
}